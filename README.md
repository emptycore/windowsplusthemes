# Windows 95 Plus! themes for Windows 10

Windows 95 Plus! themes, re-packaged for Windows 10. It changes soundschemes, desktop wallpapers, cursor and desktop icons

## Getting Started

Just double-click any themepack file to apply it to your desktop. 
You can make some yourself.\
.themepack = .cab\
.theme = .txt\
It's simple really. You just need to find a way to coble a .cab file together

## Acknowledgments

All trademarks are properties of their respective owners. Yada-Yada.

